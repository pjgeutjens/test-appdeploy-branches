$here = Split-Path -Parent $MyInvocation.MyCommand.Path

#region mock-setup
<# Mock stuff for pester, not really used anymore in the tests, but I'll leave it for reference
 # Code below mocks all funtions in the Appdeploy Toolkit matching a set of verbs

$currentFunctions = Get-ChildItem function:
$null = . $here\AppDeployToolkit\AppDeployToolkitMain.ps1
$scriptFunctions = Get-ChildItem function: | Where-Object { $currentFunctions -notcontains $_ }

$mockableVerbs = @('Execute', 'Copy', "Install", "New", "Remove", "Set", "Start", "Stop")
$mockableFunctions = @()

foreach($f in $scriptFunctions) {
            if ($null -ne ($mockableVerbs | ? { $f.Name -match $_ })) {
                $mockableFunctions += $f.Name
            }  
}

#>
#endregion


Describe "Quick Fails" {

    foreach ($f in $mockableFunctions) {
        # Write-Host "Mocking $f"
        Mock $f
    }
    
    Context "Code Sanity" {

        It "PSParser" {
            $contents = Get-Content -Path "$here\Deploy-Application.ps1" -ErrorAction Stop
            $errors = $null
            [System.Management.Automation.PSParser]::Tokenize($contents, [ref]$errors) 
            $errors | Should benullorempty
        }

        It "ScriptAnalyzer" {
            Invoke-ScriptAnalyzer -Path "$here\Deploy-Application.ps1" -ExcludeRule ("PSAvoidUsingEmptyCatchBlock","PSAvoidUsingCmdletAliases") | Should benullorempty
        }
        
        It "No source files" {
            # only the .cs file of the Appdeploy toolkit is allowed
            $suspects = Get-ChildItem -Path $here\* -Recurse -Include *.wbt, *.WBT.autosave, *.WBT.backup, *.cs, *.sln | Where-Object {$_.Name -ne "AppDeployToolkitMain.cs"}
            $suspects | Should benullorempty 
        }

    }
}